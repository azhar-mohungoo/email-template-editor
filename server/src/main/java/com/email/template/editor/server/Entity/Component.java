package com.email.template.editor.server.Entity;

import com.email.template.editor.server.Enums.ComponentType;

import javax.persistence.*;

@Entity
@Table(name = "component")
public class Component {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "component_type", nullable = false)
    private ComponentType componentType;

    @Column(name = "markUp", nullable = false)
    private String markUp;

    public Component() {
    }

    public Component(String name, String description, ComponentType componentType, String markUp) {
        this.name = name;
        this.description = description;
        this.componentType = componentType;
        this.markUp = markUp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public String getMarkUp() {
        return markUp;
    }

    public void setMarkUp(String markUp) {
        this.markUp = markUp;
    }
}
