package com.email.template.editor.server.ResponseBody;

public class UpdateComponentResponse {

    String name;
    String message;
    boolean success;

    public UpdateComponentResponse(String name, String message, boolean success) {
        this.name = name;
        this.message = message;
        this.success = success;
    }

    public UpdateComponentResponse() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
