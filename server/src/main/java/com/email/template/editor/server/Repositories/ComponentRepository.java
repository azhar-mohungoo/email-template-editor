package com.email.template.editor.server.Repositories;

import com.email.template.editor.server.Entity.Component;
import com.email.template.editor.server.Enums.ComponentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface ComponentRepository extends JpaRepository<Component, Long> {
    ArrayList<Component> findAllByComponentType(ComponentType type);
    Optional<Component> findByName(String name);
}
