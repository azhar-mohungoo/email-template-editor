package com.email.template.editor.server.RequestBody;

public class UpdateTemplateRequestBody {
    private Long id;
    private String name;
    private String description;
    private String htmlMarkup;
    private String jsonConfig;

    public UpdateTemplateRequestBody() {
    }

    public UpdateTemplateRequestBody(Long id, String name, String description, String htmlMarkup, String jsonConfig) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.htmlMarkup = htmlMarkup;
        this.jsonConfig = jsonConfig;
    }

    public UpdateTemplateRequestBody(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getHtmlMarkup() {
        return htmlMarkup;
    }

    public void setHtmlMarkup(String htmlMarkup) {
        this.htmlMarkup = htmlMarkup;
    }

    public String getJsonConfig() {
        return jsonConfig;
    }

    public void setJsonConfig(String jsonConfig) {
        this.jsonConfig = jsonConfig;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
