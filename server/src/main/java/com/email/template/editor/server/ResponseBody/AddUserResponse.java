package com.email.template.editor.server.ResponseBody;

public class AddUserResponse {

    String fullName;
    String message;
    boolean success;

    public AddUserResponse() {
    }

    public AddUserResponse(String fullName, String message, boolean success) {
        this.fullName = fullName;
        this.message = message;
        this.success = success;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
