package com.email.template.editor.server.RequestBody;

public class DeleteComponentRequestBody {

    Long id;

    public DeleteComponentRequestBody(Long id) {
        this.id = id;
    }

    public DeleteComponentRequestBody() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
