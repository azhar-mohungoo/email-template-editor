package com.email.template.editor.server.ResponseBody;

import com.email.template.editor.server.Entity.Component;

import java.util.ArrayList;

public class GetComponentsOfTypeResponse {

    ArrayList<Component> components;
    String message;
    boolean success;

    public GetComponentsOfTypeResponse(ArrayList<Component> components, String message, boolean success) {
        this.components = components;
        this.message = message;
        this.success = success;
    }

    public GetComponentsOfTypeResponse() {
    }

    public ArrayList<Component> getComponent() {
        return components;
    }

    public void setComponent(ArrayList<Component> components) {
        this.components = components;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
