package com.email.template.editor.server.RequestBody;

import com.email.template.editor.server.Enums.ComponentType;

public class AddComponentRequestBody {

    String name;
    String description;
    ComponentType componentType;
    String markUp;

    public AddComponentRequestBody(String name, String description, ComponentType componentType, String markUp) {
        this.name = name;
        this.description = description;
        this.componentType = componentType;
        this.markUp = markUp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public String getMarkUp() {
        return markUp;
    }

    public void setMarkUp(String markUp) {
        this.markUp = markUp;
    }
}
