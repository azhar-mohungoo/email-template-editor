package com.email.template.editor.server.Controllers;

import com.email.template.editor.server.Entity.User;
import com.email.template.editor.server.Repositories.UserRepository;
import com.email.template.editor.server.RequestBody.AddUserRequestBody;
import com.email.template.editor.server.RequestBody.LoginRequestBody;
import com.email.template.editor.server.ResponseBody.AddUserResponse;
import com.email.template.editor.server.Utilities.CryptographyUtility;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;
import java.util.HashSet;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final UserRepository userRepository;

    public AuthController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/addUser")
    public ResponseEntity addUser(@RequestBody AddUserRequestBody addUserRB) {
        AddUserResponse userResponse;

        try {
            String encryptedPassword = CryptographyUtility.encrypt(addUserRB.getPassword(), addUserRB.getEmail());
            User newUser = new User(addUserRB.getFullname(), addUserRB.getEmail(), encryptedPassword, new HashSet<>());
            Optional<User> doesUserExist = userRepository.findUserByEmail(newUser.getEmail());

            if (doesUserExist.isPresent()) {
                throw new EntityExistsException();
            }

            newUser = userRepository.save(newUser);
            userResponse = new AddUserResponse(newUser.getFullName(), "Create User", true);
        } catch (Exception ex) {
            userResponse = new AddUserResponse(addUserRB.getFullname(), ex.getMessage(), false);
        }

        return ResponseEntity.ok(userResponse);
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody LoginRequestBody loginRequestBody) throws Exception {
        Optional<User> user = userRepository.findUserByEmail(loginRequestBody.getEmail());

        if (user.isPresent()) {
            if (user.get().getPassword().equals(CryptographyUtility.encrypt(loginRequestBody.getPassword(), loginRequestBody.getEmail()))) {
                user.get().setPassword(loginRequestBody.getPassword());
                return ResponseEntity.ok(user);
            }
        }

        return ResponseEntity.notFound().build();
    }
}
