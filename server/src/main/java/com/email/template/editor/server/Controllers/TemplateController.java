package com.email.template.editor.server.Controllers;

import com.email.template.editor.server.Entity.Component;
import com.email.template.editor.server.Entity.Template;
import com.email.template.editor.server.Repositories.ComponentRepository;
import com.email.template.editor.server.Repositories.TemplateRepository;
import com.email.template.editor.server.RequestBody.DeleteTemplateRequestBody;
import com.email.template.editor.server.RequestBody.UpdateTemplateRequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class TemplateController {

    @Autowired
    private TemplateRepository templateRepository;
    @Autowired
    private ComponentRepository componentRepository;


    @GetMapping("/templates")
    public List<Template> getAllTemplates() {
        return templateRepository.findAll();
    }

    @GetMapping("/template")
    public ResponseEntity<Template> getTemplateById(@RequestParam("id") Long templateId) throws Exception {
        ResponseEntity response;
        Optional<Template> template = templateRepository.findById(templateId);

        if(template.isPresent()) {
            response = ResponseEntity.ok().body(template);
        } else {
            response = ResponseEntity.notFound().build();
        }
        return response;
    }

    @PostMapping("/template")
    public ResponseEntity createTemplate(@RequestBody UpdateTemplateRequestBody createTemplate) {
        String response;
        Optional<Template> newTemplate =  templateRepository.findTemplateByName(createTemplate.getName());
        if(newTemplate.isPresent()) {
            return ResponseEntity.badRequest().body("Template with name: ".concat(createTemplate.getName()).concat(" already exists."));
        }

        try {
            newTemplate = Optional.of(new Template(createTemplate.getName(), createTemplate.getDescription(), createTemplate.getJsonConfig(), createTemplate.getHtmlMarkup()));
            templateRepository.save(newTemplate.get());
            response = "Template added Successfully";
            return ResponseEntity.ok().body(templateRepository.findAll());

        } catch (Exception err) {
            response = "Template could not be added error: " + err.getMessage();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PutMapping("/template")
    public ResponseEntity updateTemplate(@RequestBody UpdateTemplateRequestBody updateTemplate) {
        String response;
        Optional<Template> template = templateRepository.findById(updateTemplate.getId());

        if (template.isPresent()) {
            template.get().setDescription(updateTemplate.getDescription());
            template.get().sethtmlMarkup(updateTemplate.getHtmlMarkup());
            template.get().setJsonConfig(updateTemplate.getJsonConfig());
            template.get().setName(updateTemplate.getName());
        }

        try {
            templateRepository.save(template.get());
            return ResponseEntity.ok().body(templateRepository.findAll());
        } catch (Exception err) {
            response = "Template could not be updated error: " + err.getMessage();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @DeleteMapping("/template")
    public ResponseEntity deleteTemplate(@RequestParam(value = "id") Long templateId) {
        Optional<Template> template = templateRepository.findById(templateId);
        String response;

        try {
            templateRepository.delete(template.get());
            return ResponseEntity.ok().body(templateRepository.findAll());
        } catch (Exception err) {
            response = "Could not delete: " + err.getMessage();
            return ResponseEntity.badRequest().body(response);
        }
    }

    /**
     * Helper function to check if selected components exist
     **/
    public void verifyComponents(Set<Component> components) throws Exception {
        for (Component component : components) {
            if (!componentRepository.existsById(component.getId()))
                throw new Exception("Component not found for Id: " + component.getId());
        }
    }

}
