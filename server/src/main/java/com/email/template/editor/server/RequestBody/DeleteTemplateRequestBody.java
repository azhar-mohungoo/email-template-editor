package com.email.template.editor.server.RequestBody;

public class DeleteTemplateRequestBody {
    private Long id;

    public DeleteTemplateRequestBody(){

    }

    public DeleteTemplateRequestBody(Long id){
        this.id = id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
