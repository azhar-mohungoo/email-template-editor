package com.email.template.editor.server.ResponseBody;

public class DeleteComponentResponse {
    String name;
    String message;
    boolean success;

    public DeleteComponentResponse() {
    }


    public DeleteComponentResponse(String name, String message, boolean success) {
        this.name = name;
        this.message = message;
        this.success = success;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
