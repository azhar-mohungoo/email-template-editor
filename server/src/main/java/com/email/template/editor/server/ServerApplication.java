package com.email.template.editor.server;

import com.email.template.editor.server.Controllers.AuthController;
import com.email.template.editor.server.Controllers.ComponentController;
import com.email.template.editor.server.Controllers.TemplateController;
import com.email.template.editor.server.Enums.ComponentType;
import com.email.template.editor.server.RequestBody.AddComponentRequestBody;
import com.email.template.editor.server.RequestBody.AddUserRequestBody;
import com.email.template.editor.server.RequestBody.UpdateTemplateRequestBody;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

    @Bean
    public CommandLineRunner demoData(AuthController authController, TemplateController tempController) {
        String html1 = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"+
                "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/> "+
                "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"/><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>"+
                " <meta name=\"format-detection\" content=\"telephone=no;address=no;email=no\"/> <title>Template1</title> <style type=\"text/css\"> "+
                "*{-webkit-font-smoothing: antialiased;}body{Margin: 0; padding: 0; min-width: 100%; color: #000000; font-family: Arial, sans-serif;"+
                " -webkit-font-smoothing: antialiased; mso-line-height-rule: exactly;}table{border-spacing: 0; color: #000000; font-family: Arial, "+
                "sans-serif;}a[href^=\"tel\"], a[href^=\"sms\"], a[href^=\"mailto\"], a{color: inherit; text-decoration: none; pointer-events: none;"+
                " cursor: default;}img{border: 0;}.wrapper{width: 100%; table-layout: fixed; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: "+
                "100%;}.webkit{max-width: 600px;}.outer{Margin: 0 auto; width: 100%; max-width: 600px;}.inner"+
                "{padding: 10px;}p{Margin: 0;}.one-column .contents{text-align: left; font-family: Arial, sans-serif; -webkit-font-smoothing: antialiased;}.one-column p{font-size: 14px; Margin-bottom: 2px; font-family: Arial, sans-serif; -webkit-font-smoothing: antialiased;}</style>"+
                "<!--[if (gte mso 9)|(IE)]> <style type=\"text/css\"> table{border-collapse: collapse !important;}</style><![endif]--></head><body style=\"Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#FFFFFF;\"><center class=\"wrapper\" style=\"width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#FFFFFF;\"> <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"background-color:#FFFFFF;\" bgcolor=\"#FFFFFF;\"> <tr> <td width=\"100%\"> <div class=\"webkit\" style=\"max-width:600px;Margin:0 auto;\"><!--[if (gte mso 9)|(IE)]> <table width=\"600\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-spacing:0;font-family: Calibri, sans-serif;color:#000000;\" > <tr> <td style=\"padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;\" ><![endif]--> <table class=\"outer\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-spacing:0;font-family: Arial, sans-serif;color:#000000;Margin:0 auto;width:100%;max-width:600px;\" bgcolor=\"#FFFFFF\"> <tr> <td style=\"padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;\"> <table class=\"one-column\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-spacing:0;font-family: Arial, sans-serif;color:#000000;max-width: 600px;background-color: #FFFFFF;\" bgcolor=\"#FFFFFF\"> <tr> <td align=\"left\" style=\"color: #000000; font-size: 16px; line-height: 18px; text-align:left;\"> <div class=\"column\" style=\"width:100%;max-width:600px;display:inline-block;vertical-align:top;direction:ltr;\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"border-spacing:0;font-family: Arial, sans-serif;color:#000000;\"> <tr> <td class=\"inner\" style=\"padding:0px; border:0;\"> <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"contents\" style=\"border-spacing:0;width:100%;font-size:16;text-align:left;\" bgcolor=\"#FFFFFF\"> <tr> <td class=\"inner\" style=\"padding:0px;\"> <p width=\"100%\" style=\"color:#000000; text-decoration: none; font-style:normal; font-size: 16px; font-weight:normal; line-height:18px; font-family:Arial, sans-serif; text-align:left;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Turpis cursus in hac habitasse platea dictumst quisque. Amet consectetur adipiscing elit ut aliquam purus sit amet luctus. Consectetur a erat nam at lectus urna duis. Interdum velit laoreet id donec ultrices tincidunt arcu. </p></td></tr></table> </td></tr></table> </div></td></tr></table> </td></tr></table><!--[if (gte mso 9)|(IE)]> </td></tr></table><![endif]--> </div></td></tr></table></center></body></html>";
        String jsonConfig = "{title: \"Default template 1\", width: 600, children: [{type: \"container\", settings: [{backgroundColor: \"black\", color: \"white\"}], children: [{type: \"text\", value: \"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Turpis cursus in hac habitasse platea dictumst quisque. Amet consectetur adipiscing elit ut aliquam purus sit amet luctus. Consectetur a erat nam at lectus urna duis. Interdum velit laoreet id donec ultrices tincidunt arcu. \"}]}]};";

        return args -> {
            authController.addUser(new AddUserRequestBody("Azhar Mohungoo", "azhar.mohungoo@entelect.co.za", "azhar"));
            authController.addUser(new AddUserRequestBody("Gift Sefako", "gift.sefako@entelect.co.za", "gift"));
            authController.addUser(new AddUserRequestBody("Maripe Makgolane", "maripe.makgolane@entelect.co.za", "maripe"));
            authController.addUser(new AddUserRequestBody("Neo Padi", "neo.padi@entelect.co.za", "neo"));
            authController.addUser(new AddUserRequestBody("Ahmad Mahomed", "ahmad.mahomed@entelect.co.za", "ahmad"));

            tempController.createTemplate(new UpdateTemplateRequestBody((long)5, "Default template 1", "System default template with minimum components", html1, jsonConfig));
            tempController.createTemplate(new UpdateTemplateRequestBody((long)6, "Default template 2", "System default template with minimum components", html1, jsonConfig));
            tempController.createTemplate(new UpdateTemplateRequestBody((long)7, "Default template 3", "System default template with minimum components", html1, jsonConfig));
        };
    }
}
