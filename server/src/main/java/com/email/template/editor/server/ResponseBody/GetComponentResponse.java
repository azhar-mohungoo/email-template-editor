package com.email.template.editor.server.ResponseBody;

import com.email.template.editor.server.Entity.Component;

import java.util.Optional;

public class GetComponentResponse {

    Component component;
    String message;
    boolean success;

    public GetComponentResponse() {
    }

    public GetComponentResponse(Component component, String message, boolean success) {
        this.component = component;
        this.message = message;
        this.success = success;
    }

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
