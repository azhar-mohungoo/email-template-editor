package com.email.template.editor.server.Entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "template")
public class Template {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Lob
    @Column(name = "jsonConfig")
    private String jsonConfig;

    @Lob
    @Column(name = "htmlMarkup", nullable = false)
    private String htmlMarkup;

    public Template(String name, String description, String jsonConfig, String htmlMarkup) {
        this.name = name;
        this.description = description;
        this.jsonConfig = jsonConfig;
        this.htmlMarkup = htmlMarkup;
    }

    public Template() {
    }

    public Template(String name, String description) {
        this.name = name;
        this.description = description;
    }
    public Template(String name, String description, Set<Component> componentSet) {
        this.name = name;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJsonConfig() {
        return jsonConfig;
    }

    public void setJsonConfig(String jsonConfig) {
        this.jsonConfig = jsonConfig;
    }

    public String gethtmlMarkup() {
        return htmlMarkup;
    }

    public void sethtmlMarkup(String htmlMarkup) {
        this.htmlMarkup = htmlMarkup;
    }

}
