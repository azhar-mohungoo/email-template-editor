package com.email.template.editor.server.RequestBody;

import com.email.template.editor.server.Enums.ComponentType;

public class UpdateComponentRequestBody {

    Long id;
    String name;
    String description;
    ComponentType componentType;
    String markUp;

    public UpdateComponentRequestBody() {
    }

    public UpdateComponentRequestBody(Long id, String name, String description, ComponentType componentType, String markUp) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.componentType = componentType;
        this.markUp = markUp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(ComponentType componentType) {
        this.componentType = componentType;
    }

    public String getMarkUp() {
        return markUp;
    }

    public void setMarkUp(String markUp) {
        this.markUp = markUp;
    }
}
