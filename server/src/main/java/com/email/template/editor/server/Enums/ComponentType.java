package com.email.template.editor.server.Enums;

public enum ComponentType {
    HEADER,
    BODY,
    FOOTER
}
