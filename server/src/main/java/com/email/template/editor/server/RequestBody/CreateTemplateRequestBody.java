package com.email.template.editor.server.RequestBody;

import com.email.template.editor.server.Entity.Component;

import java.util.Set;

public class CreateTemplateRequestBody {
    private Long id;
    private String name;
    private String description;
    private Set<Component> components;

    public CreateTemplateRequestBody(Long id, String name, String description, Set<Component> components){
        this.id = id;
        this.name = name;
        this.description = description;
        this.components = components;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Set<Component> getComponents() {
        return components;
    }

    public void setComponents(Set<Component> components) {
        this.components = components;
    }
}
