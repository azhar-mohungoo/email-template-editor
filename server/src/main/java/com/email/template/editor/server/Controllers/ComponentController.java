package com.email.template.editor.server.Controllers;

import com.email.template.editor.server.Entity.Component;
import com.email.template.editor.server.Enums.ComponentType;
import com.email.template.editor.server.Repositories.ComponentRepository;
import com.email.template.editor.server.RequestBody.AddComponentRequestBody;
import com.email.template.editor.server.RequestBody.DeleteComponentRequestBody;
import com.email.template.editor.server.RequestBody.UpdateComponentRequestBody;
import com.email.template.editor.server.ResponseBody.*;
import com.google.gson.Gson;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityExistsException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/component")
public class ComponentController {

    private final ComponentRepository componentRepository;

    public ComponentController(ComponentRepository componentRepository) {
        this.componentRepository = componentRepository;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getAllComponents() {
        HashMap<String, ArrayList> components = new HashMap<>();

        components.put(ComponentType.HEADER.toString().toLowerCase(), componentRepository.findAllByComponentType(ComponentType.HEADER));
        components.put(ComponentType.BODY.toString().toLowerCase(), componentRepository.findAllByComponentType(ComponentType.BODY));
        components.put(ComponentType.FOOTER.toString().toLowerCase(), componentRepository.findAllByComponentType(ComponentType.FOOTER));

        return new Gson().toJson(components);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<GetComponentResponse> getComponentById(@PathVariable(value = "id") Long componentId) {
        GetComponentResponse response;

        try {
            Optional<Component> component = componentRepository.findById(componentId);
            response = new GetComponentResponse(component.get(), "Successfully retrieved component with id: " + componentId, true);
        } catch (Exception ex) {
            response = new GetComponentResponse(null, ex.getMessage(), false);
        }

        return ResponseEntity.ok().body(response);
    }

    @RequestMapping(value = "/type/{componentType}", method = RequestMethod.GET)
    public ResponseEntity<GetComponentsOfTypeResponse> getComponentsByTypes(@PathVariable(value = "componentType") ComponentType componentType) {
        GetComponentsOfTypeResponse response;

        try {
            ArrayList components = componentRepository.findAllByComponentType(componentType);
            String message;

            if (components.isEmpty()) {
                message = "No components matched the following type: " + componentType.toString().toLowerCase();
            } else {
                message = "Successfully retrieved components of type: " + componentType.toString().toLowerCase();
            }

            response = new GetComponentsOfTypeResponse(components, message, true);
        } catch (Exception ex) {
            response = new GetComponentsOfTypeResponse(null, ex.getMessage(), false);
        }

        return ResponseEntity.ok().body(response);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<AddComponentResponse> createComponent(@RequestBody AddComponentRequestBody addComponentRB) throws EntityExistsException {
        AddComponentResponse response;

        try {
            Component newComponent = new Component(addComponentRB.getName(), addComponentRB.getDescription(), addComponentRB.getComponentType(), addComponentRB.getMarkUp());
            Optional<Component> doesComponentExist = componentRepository.findByName(newComponent.getName());

            if (doesComponentExist.isPresent()) {
                throw new EntityExistsException();
            }

            componentRepository.save(newComponent);
            response = new AddComponentResponse(newComponent.getName(), "Successfully created component: " + newComponent.getName(), true);
        } catch (Exception ex) {
            response = new AddComponentResponse(addComponentRB.getName(), ex.getMessage(), false);
        }

        return ResponseEntity.ok().body(response);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<UpdateComponentResponse> updateComponent(@RequestBody UpdateComponentRequestBody updateComponentRB) {
        UpdateComponentResponse response;

        try {
            Component newComponent = new Component(updateComponentRB.getName(), updateComponentRB.getDescription(), updateComponentRB.getComponentType(), updateComponentRB.getMarkUp());
            Optional<Component> existingComponent = componentRepository.findById(updateComponentRB.getId());

            existingComponent.get().setName(newComponent.getName());
            existingComponent.get().setDescription(newComponent.getDescription());
            existingComponent.get().setComponentType(newComponent.getComponentType());
            existingComponent.get().setMarkUp(newComponent.getMarkUp());

            componentRepository.save(existingComponent.get());
            response = new UpdateComponentResponse(newComponent.getName(), "Successfully updated component: " + newComponent.getName(), true);
        } catch (Exception ex) {
            response = new UpdateComponentResponse(updateComponentRB.getName(), ex.getMessage(), false);
        }

        return ResponseEntity.ok().body(response);
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public ResponseEntity<DeleteComponentResponse> deleteComponent(@RequestBody DeleteComponentRequestBody deleteComponentRB) {
        DeleteComponentResponse response;

        try {
            Long componentId = deleteComponentRB.getId();
            Optional<Component> component = componentRepository.findById(componentId);
            componentRepository.deleteById(componentId);
            response = new DeleteComponentResponse(component.get().getName(), "Component successfully deleted", true);
        } catch (Exception ex) {
            response = new DeleteComponentResponse(null, ex.getMessage(), false);
        }

        return ResponseEntity.ok().body(response);
    }


}
