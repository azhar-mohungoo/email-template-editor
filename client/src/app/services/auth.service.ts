import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable()
export class AuthService {
  url = `${environment.baseUrl}/api/auth`;

  constructor(private httpClient: HttpClient) {
  }

  addUser(user: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.post(`${this.url}/addUser`, {
      fullname: user.fullname,
      email: user.email,
      password: user.password
    }, httpOptions);
  }

  login(credentials: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.post(`${this.url}/login`, {
      email: credentials.email,
      password: credentials.password
    }, httpOptions);
  }
}
