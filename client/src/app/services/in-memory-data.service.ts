import {InMemoryDbService} from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const customerTableMetadata = [
      {id: 1, value: 'firstName'},
      {id: 2, value: 'lastName'},
      {id: 3, value: 'title'},
      {id: 4, value: 'email'},
      {id: 5, value: 'address'}
    ];

    return {
      data: {
        customerTableMetadata
      }
    };
  }
}
