import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {tap} from 'rxjs/internal/operators/tap';

@Injectable()
export class ComponentService {
  url = `${environment.baseUrl}/api/component`;

  constructor(private httpClient: HttpClient) {
  }

  getAllComponents(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.get(this.url, httpOptions);
  }

  getComponent(id: number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.get(`${this.url}/${id}`, httpOptions);
  }

  getComponentsByType(type: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.get(`${this.url}/type/${type}`, httpOptions);
  }

  createComponent(component: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.post(this.url, component, httpOptions);
  }

  updateComponent(component: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.put(this.url, component, httpOptions);
  }

  deleteComponent(id: number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.httpClient.delete(`${this.url}/${id}`, httpOptions);
  }

  getCustomerTableMetadata(filter: { value: string } = {value: ''}): Observable<any> {
    return this.httpClient.get<any>('/api/data')
      .pipe(
        tap((response: any) => {
          response.customerTableMetadata = response.customerTableMetadata
            .filter(result => result.value.includes(filter.value));

          return response;
        })
      );
  }
}
