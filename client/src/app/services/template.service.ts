import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class TemplateService {
  url = `${environment.baseUrl}/api/template`;

  constructor(private httpClient: HttpClient) {
  }

  getAllTemplates(): Observable<any> {
    return this.httpClient.get(`${this.url}s`);
  }

  getTemplate(id: number): Observable<any> {
    return this.httpClient.get(`${this.url}?id=${id}`);
  }

  createTemplate(template: any): Observable<any> {
    return this.httpClient.post(this.url, template);
  }

   updateTemplate(template: any): Observable<any> {
     return this.httpClient.put(this.url, template);
   }

  deleteTemplate(id: number): Observable<any> {
    return this.httpClient.delete(`${this.url}?id=${id}`);
  }
}
