import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-pre-header-config',
  templateUrl: './pre-header-config.component.html',
  styleUrls: ['./pre-header-config.component.scss']
})
export class PreHeaderConfigComponent implements OnInit {

  preHeaderConfigForm = new FormGroup({
    type: new FormControl('pre-header', [Validators.required]),
    value: new FormControl(new String()),
  })

  getType(): any {
    return this.preHeaderConfigForm.get('type');
  }

  getValue(): any{
    return this.preHeaderConfigForm.get('value');
  }
  constructor() { }

  addElement() {
    return{
      "type": this.getType().value,
      "value": this.getType().value,
      "settings": {},
      "children": []
    };
  }
  ngOnInit() {
  }

}
