import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreHeaderConfigComponent } from './pre-header-config.component';

describe('PreHeaderConfigComponent', () => {
  let component: PreHeaderConfigComponent;
  let fixture: ComponentFixture<PreHeaderConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreHeaderConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreHeaderConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
