import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-table-config',
  templateUrl: './table-config.component.html',
  styleUrls: ['./table-config.component.scss']
})
export class TableConfigComponent implements OnInit {

  tableConfigForm = new FormGroup({
    type: new FormControl('table', [Validators.required]),
    width: new FormControl(100),
  });

  constructor() {
  }

  ngOnInit() {
  }

  getType(): any {
    return this.tableConfigForm.get('type');
  }

  getWidth(): any {
    return this.tableConfigForm.get('width');
  }

  addElement() {
    // console.log(this.tableConfigForm.getRawValue());

    return{
      "type": this.getType().value,
      "width": this.getWidth().value,
      "settings": {},
      "children": []
    };
  }
}
