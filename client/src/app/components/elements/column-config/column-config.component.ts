import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-column-config',
  templateUrl: './column-config.component.html',
  styleUrls: ['./column-config.component.scss']
})
export class ColumnConfigComponent implements OnInit {

  columnConfigForm = new FormGroup({
    type: new FormControl('column', [Validators.required]),
    border: new FormControl(0),
    borderRadius: new FormControl(0)
  });

  constructor() {
  }

  ngOnInit() {
  }

  getType(): any {
    return this.columnConfigForm.get('type');
  }

  getBorder(): any {
    return this.columnConfigForm.get('border');
  }

  getBorderRadius(): any {
    return this.columnConfigForm.get('borderRadius');
  }

  addElement(): any {
    return{
      "type": this.getType().value,
      "border": this.getBorder().value,
      "borderRadius": this.getBorderRadius().value,
      "settings": {},
      "children": []
    };
  }
}
