export const elements = [
  'cell-config',
  'column-config',
  // 'column-container-config',
  // 'container-config',
  'handlebars-repeater-config',
  'image-config',
  'link-config',
  'pre-header-config',
  'row-config',
  'separator-config',
  'spacing-config',
  // 'table-config',
  'text-config',
];


