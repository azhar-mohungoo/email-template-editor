import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CellConfigComponent } from './cell-config.component';

describe('CellConfigComponent', () => {
  let component: CellConfigComponent;
  let fixture: ComponentFixture<CellConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CellConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CellConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
