import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-cell-config',
  templateUrl: './cell-config.component.html',
  styleUrls: ['./cell-config.component.scss']
})
export class CellConfigComponent implements OnInit {

  cellConfigForm = new FormGroup({
    type: new FormControl('cell', [Validators.required]),
    value: new FormControl(''),
    colSpan: new FormControl(1)
  });

  constructor() {
  }

  ngOnInit() {
  }

  getType(): any {
    return this.cellConfigForm.get('type');
  }

  getValue(): any {
    return this.cellConfigForm.get('value');
  }

  getColSpan(): any {
    return this.cellConfigForm.get('colSpan');
  }

  addElement(): any {
    return {
      "type": this.getType().value,
      "value": this.getValue().value,
      "colSpan": this.getColSpan().value,
      "settings": {},
      "children": []
    };
  }
}
