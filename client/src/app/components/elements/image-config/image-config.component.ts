import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-image-config',
  templateUrl: './image-config.component.html',
  styleUrls: ['./image-config.component.scss']
})
export class ImageConfigComponent implements OnInit {

  imageConfigForm = new FormGroup({
    type: new FormControl('image', [Validators.required]),
    src: new FormControl(new String()),
    alt: new FormControl(new String()),
  })

  getType(): any {
    return this.imageConfigForm.get('type');
  }

  getSrc(): any{
    return this.imageConfigForm.get('src');
  }

  getAlt(): any{
    return this.imageConfigForm.get('alt');
  }

  addElement(): any {
    return{
      type: this.getType().value,
      src: this.getSrc().value,
      alt: this.getAlt().value
    };
  }
  constructor() { }

  ngOnInit() {
  }

}
