import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpacingConfigComponent } from './spacing-config.component';

describe('SpacingConfigComponent', () => {
  let component: SpacingConfigComponent;
  let fixture: ComponentFixture<SpacingConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpacingConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpacingConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
