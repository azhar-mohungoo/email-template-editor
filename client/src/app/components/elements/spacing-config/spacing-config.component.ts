import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-spacing-config',
  templateUrl: './spacing-config.component.html',
  styleUrls: ['./spacing-config.component.scss']
})
export class SpacingConfigComponent implements OnInit {
  spacingConfigForm = new FormGroup({
    type: new FormControl('spacing', [Validators.required]),
    spacing: new FormControl(''),
  });
  getType(): any {
    return this.spacingConfigForm.get('type');
  }
  getSpacing(): any {
    return this.spacingConfigForm.get('spacing');
  }

  addElement() {
    return{
      type: this.getType().value,
      size: this.getSpacing().value,
      settings: {},
      children: []
    };
  }
  constructor() { }

  ngOnInit() {
  }

}
