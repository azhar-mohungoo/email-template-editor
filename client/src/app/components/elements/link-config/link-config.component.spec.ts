import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkConfigComponent } from './link-config.component';

describe('LinkConfigComponent', () => {
  let component: LinkConfigComponent;
  let fixture: ComponentFixture<LinkConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
