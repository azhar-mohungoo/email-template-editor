import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-link-config',
  templateUrl: './link-config.component.html',
  styleUrls: ['./link-config.component.scss']
})
export class LinkConfigComponent implements OnInit {
  linkConfigForm = new FormGroup({
    type: new FormControl('link', [Validators.required]),
    href: new FormControl(''),
    value: new FormControl('')
  });
  getType(): any {
    return this.linkConfigForm.get('type');
  }
  getHref(): any {
    return this.linkConfigForm.get('href');
  }
  getValue(): any {
    return this.linkConfigForm.get('value');
  }

  addElement(): any {
    return{
      type: this.getType().value,
      href: this.getHref().value,
      value: this.getValue().value,
      settings: {color: '#db7447'}
    };
  }
  constructor() { }

  ngOnInit() {
  }

}
