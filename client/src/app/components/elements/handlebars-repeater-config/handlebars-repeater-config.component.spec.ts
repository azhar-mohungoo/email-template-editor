import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandlebarsRepeaterConfigComponent } from './handlebars-repeater-config.component';

describe('HandlebarsRepeaterConfigComponent', () => {
  let component: HandlebarsRepeaterConfigComponent;
  let fixture: ComponentFixture<HandlebarsRepeaterConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandlebarsRepeaterConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandlebarsRepeaterConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
