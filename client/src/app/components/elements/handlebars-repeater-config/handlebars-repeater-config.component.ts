import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-handlebars-repeater-config',
  templateUrl: './handlebars-repeater-config.component.html',
  styleUrls: ['./handlebars-repeater-config.component.scss']
})
export class HandlebarsRepeaterConfigComponent implements OnInit {

  handlebarsConfigForm = new FormGroup({
    type: new FormControl('column', [Validators.required]),
    identifier: new FormControl('identifier'),
    children: new FormControl('children')
  });

  constructor() { }

  ngOnInit() {
  }

  getType(): any {
    return this.handlebarsConfigForm.get('type');
  }

  getIdentifier(): any {
    return this.handlebarsConfigForm.get('identifier');
  }

  getChildren(): any {
    return this.handlebarsConfigForm.get('children');
  }

  addElement(): any {
    return{
      "type": this.getType().value,
      "identifier": this.getIdentifier().value,
      // "children": this.getChildren().value,
      "settings": {},
      "children": []
    };
  }
}
