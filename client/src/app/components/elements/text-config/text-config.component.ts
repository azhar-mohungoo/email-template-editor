import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-text-config',
  templateUrl: './text-config.component.html',
  styleUrls: ['./text-config.component.scss']
})
export class TextConfigComponent implements OnInit {
  textConfigForm = new FormGroup({
    type: new FormControl('text'),
    value: new FormControl('')
  });
  getType(): any {
    return this.textConfigForm.get('type');
  }
  getValue(): any {
    return this.textConfigForm.get('value');
  }

  addElement(){
    return{
      type: this.getType().value,
      value: this.getValue().value,
      settings: {color:'#db7447'}
    };
    
  }

  constructor() { }

  ngOnInit() {
  }

}
