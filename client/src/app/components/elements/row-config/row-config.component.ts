import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-row-config',
  templateUrl: './row-config.component.html',
  styleUrls: ['./row-config.component.scss']
})
export class RowConfigComponent implements OnInit {

  rowConfigForm = new FormGroup({
    type: new FormControl('row', [Validators.required])
  });

  constructor() { }

  ngOnInit() {
  }

  getType(): any {
    return this.rowConfigForm.get('type');
  }

  addElement(): any {
    return{
      "type": this.getType().value,
      "settings": {},
      "children": []
    };
  }
}
