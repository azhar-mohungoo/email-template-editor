import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RowConfigComponent } from './row-config.component';

describe('RowConfigComponent', () => {
  let component: RowConfigComponent;
  let fixture: ComponentFixture<RowConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RowConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RowConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
