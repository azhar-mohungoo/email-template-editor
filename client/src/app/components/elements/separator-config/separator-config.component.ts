import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';


@Component({
  selector: 'app-separator-config',
  templateUrl: './separator-config.component.html',
  styleUrls: ['./separator-config.component.scss']
})
export class SeparatorConfigComponent implements OnInit {
  separatorConfigForm = new FormGroup({
    type: new FormControl('separator', [Validators.required]),
    size: new FormControl(''),
    style: new FormControl(''),
    colour: new FormControl('')
  });

  size = '';

  styles = [
    {value: 'none'},
    {value: 'dotted'},
    {value: 'dashed'},
    {value: 'solid'}
  ];

  colours = [
    {value: 'black'},
    {value: 'green'},
    {value: 'yellow'},
    {value: 'blue'}
  ];
  getType(): any {
    return this.separatorConfigForm.get('type');
  }

  getSize(): any {
    return this.separatorConfigForm.get('size');
  }

  getStyle(): any {
    return this.separatorConfigForm.get('style');
  }

  getColour(): any {
    return this.separatorConfigForm.get('colour');
  }

  addElement() {
   return{
      type: this.getType().value,
      size: this.getSize().value,
      colour: this.getColour().value,
      style: this.getStyle().value,
      settings: {},
      children: []
    };
  }

  constructor() { }

  ngOnInit() {
  }
}
