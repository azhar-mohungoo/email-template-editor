import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeparatorConfigComponent } from './separator-config.component';

describe('SeparatorConfigComponent', () => {
  let component: SeparatorConfigComponent;
  let fixture: ComponentFixture<SeparatorConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeparatorConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeparatorConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
