import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnContainersConfigComponent } from './column-containers-config.component';

describe('ColumnContainersConfigComponent', () => {
  let component: ColumnContainersConfigComponent;
  let fixture: ComponentFixture<ColumnContainersConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnContainersConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnContainersConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
