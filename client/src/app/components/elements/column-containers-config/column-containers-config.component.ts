import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ColumnConfigComponent} from '../column-config/column-config.component';

@Component({
  selector: 'app-column-containers-config',
  templateUrl: './column-containers-config.component.html',
  styleUrls: ['./column-containers-config.component.scss']
})
export class ColumnContainersConfigComponent implements OnInit {

  columnContainerConfigForm = new FormGroup({
    type: new FormControl('twoColumnContainer', [Validators.required]),
    // leftColumn: new FormControl(new ColumnConfigComponent().addElement()),
    // middleColumn: new FormControl(new ColumnConfigComponent().addElement()),
    // rightColumn: new FormControl(new ColumnConfigComponent().addElement()),
    margin: new FormControl(0),
    padding: new FormControl(0),
    border: new FormControl(0),
    borderRadius: new FormControl(0)
  });

  constructor() {
  }

  ngOnInit() {
  }

  getType(): any {
    return this.columnContainerConfigForm.get('type');
  }

  getMargin(): any {
    return this.columnContainerConfigForm.get('margin');
  }

  getPadding(): any {
    return this.columnContainerConfigForm.get('padding');
  }

  getBorder(): any {
    return this.columnContainerConfigForm.get('border');
  }

  getBorderRadius(): any {
    return this.columnContainerConfigForm.get('borderRadius');
  }

  addElement(): any {
    return{
      'type': this.getType().value,
      'margin': this.getMargin().value,
      'padding': this.getPadding().value,
      'border': this.getBorder().value,
      'borderRadius': this.getBorderRadius().value,
      'settings': {},
    };
  }
}
