import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerConfigComponent } from './container-config.component';

describe('ContainerConfigComponent', () => {
  let component: ContainerConfigComponent;
  let fixture: ComponentFixture<ContainerConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
