import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-all',
  templateUrl: './view-all.component.html',
  styleUrls: ['./view-all.component.scss']
})
export class ViewAllComponent implements OnInit {

  selectedTemplate: any;
  availableTemplates: object[] = [
    {name: 'Default Template', description: 'Automatically added template component'},
    {name: 'Summer Template', description: 'A description'},
    {name: 'Winter Template', description: 'You get the idea?'}
  ];
  constructor() { }

  ngOnInit() {
  }

}
