import {Component, OnInit, Output} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-dropdown-group',
  templateUrl: './dropdown-group.component.html',
  styleUrls: ['./dropdown-group.component.scss']
})
export class DropdownGroupComponent implements OnInit {

  selectedHeader: any;
  availableHeaders: object[] = [
    {name: 'Default Header', description: 'Automatically added when creating a template'},
    {name: 'Summer Header', description: 'Summer header description'},
    {name: 'Winter Header', description: 'Winter header stuff'},
    {name: 'Disco Header', description: 'Disco header description'}
    ];


  selectedBody: any;
  availableBodies: object[] = [
    {name: 'Default Body', description: 'Automatically added body component'},
    {name: 'Summer Body', description: 'A description'},
    {name: 'Winter Body', description: 'You get the idea?'},
    {name: 'Example header', description: 'Something different'},
    {name: 'Sneakers', description: 'bruhhh'}
  ];

  selectedFooter: any;
  availableFooters: object[] = [
    {name: 'Default Footer', description: 'To be added as default footer'},
    {name: 'Summer Footer', description: 'Use me in Summer'},
    {name: 'Summer Footer v2', description: 'Me Too'},
    {name: 'Example footer', description: 'This is not static *sideEye'},
    {name: 'Sneakers', description: 'bruhhh'}
  ];
  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
  constructor() {
  }

  ngOnInit() {
  }

}
