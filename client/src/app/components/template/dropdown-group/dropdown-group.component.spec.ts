import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownGroupComponent } from './dropdown-group.component';

describe('DropdownGroupComponent', () => {
  let component: DropdownGroupComponent;
  let fixture: ComponentFixture<DropdownGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
