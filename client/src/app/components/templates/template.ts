export class Template {
  constructor(id: number, name: string, description: string, jsonConfig: any, htmlMarkup: any) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.jsonConfig = jsonConfig;
    this.htmlMarkup = htmlMarkup;
  }

  id: number;
    name: string;
    description: string;
    jsonConfig: any;
    htmlMarkup: any;
}
