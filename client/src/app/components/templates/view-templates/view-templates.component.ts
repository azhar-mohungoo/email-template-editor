import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Template } from 'src/app/components/templates/template';
import { TemplateService } from 'src/app/services/template.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-templates',
  templateUrl: './view-templates.component.html',
  styleUrls: ['./view-templates.component.scss']
})
export class ViewTemplatesComponent implements OnInit {

  templateList: Observable<Template[]>;

  constructor(private templateService: TemplateService,  private router: Router) { }

  ngOnInit() {
    this.loadTemplates();
  }

  loadTemplates() {
    this.templateList = this.templateService.getAllTemplates();
  }

  viewTemplate(id: number) {
    this.router.navigate(['manage-template', {id} ]);
  }

}
