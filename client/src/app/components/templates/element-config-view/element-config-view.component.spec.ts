import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementConfigViewComponent } from './element-config-view.component';

describe('ElementConfigViewComponent', () => {
  let component: ElementConfigViewComponent;
  let fixture: ComponentFixture<ElementConfigViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementConfigViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementConfigViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
