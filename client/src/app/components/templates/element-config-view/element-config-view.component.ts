import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-element-config-view',
  templateUrl: './element-config-view.component.html',
  styleUrls: ['./element-config-view.component.scss']
})
export class ElementConfigViewComponent implements OnInit {

  @Output() elementEmitter: EventEmitter<string> = new EventEmitter<string>();
  selectedElement: string;
  markUp: any;

  constructor() {
  }

  ngOnInit() {
  }

  retrieveElement(event: any) {
    this.selectedElement = event;
    // console.log(this.selectedElement);
    // this.elementEmitter.emit(this.selectedElement);

    switch (this.selectedElement) {
      case('cell-config'):
        this.markUp = 'cell-config';
      case'column-config':
        this.markUp = 'column-config';
      case'handlebars-repeater-config':
        this.markUp = 'handlebars-repeater-config';
      case'image-config':
        this.markUp = 'image-config';
      case'link-config':
        this.markUp = 'link-config';
      case'pre-header-config':
        this.markUp = 'pre-header-config';
      case'row-config':
        this.markUp = 'row-config';
      case'separator-config':
        this.markUp = 'separator-config';
      case'spacing-config':
        this.markUp = 'spacing-config';
      case'text-config':
        this.markUp = 'text-config';
    }
  }

  formatOption(element: string) {
    let formattedOption = element;

    formattedOption = formattedOption.replace('-config', '');
    formattedOption = formattedOption.replace('-', ' ');
    return formattedOption.charAt(0).toUpperCase() + formattedOption.slice(1);
  }

}
