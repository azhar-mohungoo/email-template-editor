import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {elements} from './../../elements';

@Component({
  selector: 'app-element-selector',
  templateUrl: './element-selector.component.html',
  styleUrls: ['./element-selector.component.scss']
})
export class ElementSelectorComponent implements OnInit {

  elements: String[] = elements;
  selectedElement: string;
  @Output() elementEmitter: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit() {
  }

  onChange(element: any) {
    this.selectedElement = element.value;
    // console.log(this.selectedElement);
    this.elementEmitter.emit(this.selectedElement);
  }

  formatOption(element: string) {
    let formattedOption = element;

    formattedOption = formattedOption.replace('-config', '');
    formattedOption = formattedOption.replace('-', ' ');
    return formattedOption.charAt(0).toUpperCase() + formattedOption.slice(1);
  }

}
