import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {debounceTime, finalize} from 'rxjs/operators';
import {tap} from 'rxjs/internal/operators/tap';
import {switchMap} from 'rxjs/internal/operators/switchMap';
import {ComponentService} from '../../../services/component.service';

@Component({
  selector: 'app-add-components',
  templateUrl: './add-components.component.html',
  styleUrls: ['./add-components.component.scss']
})
export class AddComponentsComponent implements OnInit {

  // htmlContent: string = '';
  //
  // config: AngularEditorConfig = {
  //   editable: true,
  //   spellcheck: true,
  //   height: '200px',
  //   placeholder: 'Enter text here...',
  //   translate: 'no',
  //   uploadUrl: 'v1/images'
  // };

  filteredCustomerTableMetadata: any[] = [];
  customerTableMetadataForm: FormGroup;
  isLoading = false;

  constructor(private componentService: ComponentService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.customerTableMetadataForm = this.formBuilder.group({
      customerTableMetadataInput: null
    });

    this.customerTableMetadataForm
      .get('customerTableMetadataInput')
      .valueChanges
      .pipe(
        debounceTime(300),
        tap(() => this.isLoading = true),
        switchMap(value => this.componentService.getCustomerTableMetadata({value})
          .pipe(
            finalize(() => this.isLoading = false),
          )
        )
      )
      .subscribe(result => this.filteredCustomerTableMetadata = result.customerTableMetadata);
  }

  display(result: any) {
    if (result) {
      return result.value;
    }
  }
}
