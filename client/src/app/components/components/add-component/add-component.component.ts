import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-add-component',
  templateUrl: './add-component.component.html',
  styleUrls: ['./add-component.component.scss']
})
export class AddComponentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  Types: Type[] = [
    {value: 'header-1', viewValue: 'header'},
    {value: 'body-1', viewValue: 'body'},
    {value: 'footer-1', viewValue: 'footer'}
  ]
}

export interface Type{
  value: String;
  viewValue:String;
}
