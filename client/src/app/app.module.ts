import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './components/auth/login/login.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSelectModule,
  MatToolbarModule,
} from '@angular/material';
import {AuthService} from './services/auth.service';
import {ComponentService} from './services/component.service';
import {TemplateService} from './services/template.service';
import {HttpClientModule} from '@angular/common/http';
import {ComponentsComponent} from './components/components/components.component';
import {AddComponentsComponent} from './components/components/add-components/add-components.component';
import {AngularEditorModule} from '@kolkov/angular-editor';
import {DropdownGroupComponent} from './components/template/dropdown-group/dropdown-group.component';
import {ComponentListItemComponent} from './components/template/dropdown-group/component-list-item/component-list-item.component';
import {SelectedComponentsComponent} from './components/template/selected-components/selected-components.component';
import {TemplatePreviewComponent} from './components/template/template-preview/template-preview.component';
import {TemplateViewComponent} from './components/template/template-view/template-view.component';
import {AddComponentComponent} from './components/components/add-component/add-component.component';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './services/in-memory-data.service';
import {ImageConfigComponent} from './components/elements/image-config/image-config.component';
import {ContainerConfigComponent} from './components/elements/container-config/container-config.component';
import {TextConfigComponent} from './components/elements/text-config/text-config.component';
import {LinkConfigComponent} from './components/elements/link-config/link-config.component';
import {SeparatorConfigComponent} from './components/elements/separator-config/separator-config.component';
import {SpacingConfigComponent} from './components/elements/spacing-config/spacing-config.component';
import {TableConfigComponent} from './components/elements/table-config/table-config.component';
import {RowConfigComponent} from './components/elements/row-config/row-config.component';
import {CellConfigComponent} from './components/elements/cell-config/cell-config.component';
import {PreHeaderConfigComponent} from './components/elements/pre-header-config/pre-header-config.component';
import {HandlebarsRepeaterConfigComponent} from './components/elements/handlebars-repeater-config/handlebars-repeater-config.component';
import {ColumnContainersConfigComponent} from './components/elements/column-containers-config/column-containers-config.component';
import {ColumnConfigComponent} from './components/elements/column-config/column-config.component';
import {TemplatesComponent} from './components/templates/templates.component';
import {CreateTemplatesComponent} from './components/templates/create-templates/create-templates.component';
import {UpdateTemplatesComponent} from './components/templates/update-templates/update-templates.component';
import {ElementSelectorComponent} from './components/templates/element-selector/element-selector.component';
import {ElementConfigViewComponent} from './components/templates/element-config-view/element-config-view.component';
import {NgxJsonViewerModule} from 'ngx-json-viewer';
import { ViewAllComponent } from './components/template/view-all/view-all.component';
import { ViewTemplatesComponent } from './components/templates/view-templates/view-templates.component';

const MATERIAL_DESIGN_IMPORTS = [

  MatFormFieldModule,
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatMenuModule,
  MatIconModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatListModule,
  MatGridListModule,
  MatAutocompleteModule,
  MatSelectModule,
  MatListModule,
  MatRadioModule,
  MatGridListModule,
  MatProgressSpinnerModule
];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotFoundComponent,
    ComponentsComponent,
    DropdownGroupComponent,
    AddComponentsComponent,
    AddComponentComponent,
    ComponentListItemComponent,
    SelectedComponentsComponent,
    TemplatePreviewComponent,
    TemplateViewComponent,
    ImageConfigComponent,
    ContainerConfigComponent,
    TextConfigComponent,
    LinkConfigComponent,
    SeparatorConfigComponent,
    SpacingConfigComponent,
    TableConfigComponent,
    RowConfigComponent,
    CellConfigComponent,
    PreHeaderConfigComponent,
    HandlebarsRepeaterConfigComponent,
    ColumnContainersConfigComponent,
    ColumnConfigComponent,
    TemplatesComponent,
    CreateTemplatesComponent,
    UpdateTemplatesComponent,
    ElementSelectorComponent,
    ElementConfigViewComponent,
    ViewTemplatesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    FlexLayoutModule,
    AngularEditorModule,
    // NgxJsonViewModule,
    NgxJsonViewerModule,
    [...MATERIAL_DESIGN_IMPORTS]// ,
    // HttpClientInMemoryWebApiModule.forRoot(
    //   InMemoryDataService, {dataEncapsulation: false, delay: 1000}),
  ],
  providers: [AuthService, ComponentService, TemplateService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
