import {Component} from '@angular/core';
import {AuthService} from './services/auth.service';
import * as email_editor from 'email-template-builder';
// import {forkJoin} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';
  templateConfig = {
    title: 'My e-mail template',
    width: 600,
    children: [
      {
        type: 'container',
        settings: [{backgroundColor: 'black', color: 'white'}],
        children: [
          {
            type: 'text',
            value: 'Hi {{upperCase firstName}} {{lastName}}'
          }
        ]
      }
    ]
  };
  html = email_editor.generate(this.templateConfig);
  constructor(private authService: AuthService) {
    // console.log(this.html);
  }
}
