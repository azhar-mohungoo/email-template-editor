import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/auth/login/login.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {ViewTemplatesComponent} from './components/templates/view-templates/view-templates.component';
import {CreateTemplatesComponent} from './components/templates/create-templates/create-templates.component';

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'view-templates', component: ViewTemplatesComponent},
  {path: 'manage-template', component: CreateTemplatesComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
